# Node.js on AWS with GitLab by CodeWithDragos

### Setup

1. Click in "Use this template" to clone this repository in your GitHub

2. Create a GitLab project based on the repository you have created

---

### use to clone the pipeline
NOT this: git@gitlab.com:Priti0811/gitlab_aws_ec2.git

Correct format: https://git@gitlab.com/Priti0811/gitlab_aws_ec2.git


### Getting Started :rocket:

Please install dependencies by running:

`npm install`

### Run the project

You can run the project with:

`npm start`

---

### Made with :orange_heart: in Berlin by @CodeWithDragos
